﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerController : MonoBehaviour {

	[Header("Movement Objects")]
	public Transform ModelFlyerTransform;
	public Transform CameraFollowTransform;
	public Transform CameraLookAtTransform;
	[Space(20)]

	[Header("Horizontal Rotation")]
	public float HorizontalPlayerRotationMaxLimit = 1f;
	public float HorizontalModelrRotationMaxLimit = 1f;
	public float HorizontalAutoBalanceSpeed = 1f;
	public float HorizontalInertiaValue = 0.001f;

	private float HorizontalModelRotation = 0f;

	[Header("Vertical Rotation")]
	public float VerticalPlayerRotationMaxLimit = 1f;
	public float VerticalModelrRotationMaxLimit = 1f;
	public float VerticalAutoBalanceSpeed = 1f;
	public float VerticalInertiaValue = 0.001f;

	private float VerticalModelRotation = 0f;
	[Space(20)]

	[Header("Movement settings")]
	public float PlayerMovementSpeed = 0.1f;
	public float VerticalUpMovementSpeed = 0.1f;
	public float VerticalDownMovementSpeed = 0.1f;

	public static event Action<Transform, Transform> PlayerSpawned = delegate {};
	public static event Action 						 PlayerDestroyed = delegate {};

	private bool UpPressed = false;
	private bool LeftPressed = false;
	private bool RightPressed = false;

	private bool IsGamePaused = false;
	private bool IsPlayerAlive = true;

	void Awake () {
		#if UNITY_EDITOR
		gameObject.AddComponent<KeyboardInputController>();

		KeyboardInputController.LeftPressedEvent += LeftPressedEventHandler;
		KeyboardInputController.RightPressedEvent += RightPressedEventHandler;
		KeyboardInputController.UpPressedEvent += UpPressedEventHandler;

		KeyboardInputController.LeftReleasedEvent += LeftReleasedHandler;
		KeyboardInputController.RightReleasedEvent += RightReleasedHandler;
		KeyboardInputController.UpReleasedEvent += UpReleasedHandler;
		#endif

		Camera.main.gameObject.AddComponent<CameraFollowController> ();
	}

	void Start () {
		PlayerSpawned (CameraFollowTransform, CameraLookAtTransform);
	}

	void OnDestroy() {
		#if UNITY_EDITOR
		KeyboardInputController.LeftPressedEvent -= LeftPressedEventHandler;
		KeyboardInputController.RightPressedEvent -= RightPressedEventHandler;
		KeyboardInputController.UpPressedEvent -= UpPressedEventHandler;

		KeyboardInputController.LeftReleasedEvent -= LeftReleasedHandler;
		KeyboardInputController.RightReleasedEvent -= RightReleasedHandler;
		KeyboardInputController.UpReleasedEvent -= UpReleasedHandler;
		#endif
	}

	void Update() {
		if (IsGamePaused || !IsPlayerAlive) {
			return;
		}

		transform.Translate(new Vector3(0.0f, 0.0f, PlayerMovementSpeed));

		if (UpPressed) {
			//VerticalModelRotation = Mathf.Clamp(VerticalModelRotation-VerticalInertiaValue, -VerticalPlayerRotationMaxLimit, VerticalPlayerRotationMaxLimit);
			transform.Translate (0.0f, VerticalUpMovementSpeed, 0.0f);

			//if (ModelFlyerTransform.localRotation.eulerAngles.z < 210f) {
			//	ModelFlyerTransform.Rotate (0.0f, 0.0f, VerticalModelrRotationMaxLimit);
			//}
		} else {
			transform.Translate (0.0f, -VerticalDownMovementSpeed, 0.0f);
		}

		if (LeftPressed) {
			HorizontalModelRotation = Mathf.Clamp(HorizontalModelRotation-HorizontalInertiaValue, -HorizontalPlayerRotationMaxLimit, HorizontalPlayerRotationMaxLimit);
			transform.Rotate(0.0f, HorizontalModelRotation, 0.0f);

			if (ModelFlyerTransform.localRotation.eulerAngles.z < 210f) {
				ModelFlyerTransform.Rotate (0.0f, 0.0f, HorizontalModelrRotationMaxLimit);
			}
		}

		if (RightPressed) {
			HorizontalModelRotation = Mathf.Clamp(HorizontalModelRotation+HorizontalInertiaValue, -HorizontalPlayerRotationMaxLimit, HorizontalPlayerRotationMaxLimit);
			transform.Rotate(0.0f, HorizontalModelRotation, 0.0f);
			
			if (ModelFlyerTransform.localRotation.eulerAngles.z > 150f) {
				ModelFlyerTransform.Rotate (0.0f, 0.0f, -HorizontalModelrRotationMaxLimit);
			}
		}

		if (!LeftPressed && !RightPressed) {
			float BalanceAngle = 0f;

			float CurrAngle = ModelFlyerTransform.localRotation.eulerAngles.z;

			if (CurrAngle <= 220f && CurrAngle > 179f) {
				BalanceAngle = -HorizontalAutoBalanceSpeed;
			}

			if (CurrAngle < 179 && CurrAngle >= 140f) {
				BalanceAngle = HorizontalAutoBalanceSpeed;
			}

			if (HorizontalModelRotation > HorizontalInertiaValue) {
				HorizontalModelRotation = Mathf.Clamp(HorizontalModelRotation-HorizontalInertiaValue, -HorizontalPlayerRotationMaxLimit, HorizontalPlayerRotationMaxLimit);
			} 
			if (HorizontalModelRotation < HorizontalInertiaValue) {
				HorizontalModelRotation = Mathf.Clamp(HorizontalModelRotation+HorizontalInertiaValue, -HorizontalPlayerRotationMaxLimit, HorizontalPlayerRotationMaxLimit);
			}

			transform.Rotate(0.0f, HorizontalModelRotation, 0.0f);
			ModelFlyerTransform.Rotate (0.0f, 0.0f, BalanceAngle);
		}
	}

	private void UpReleasedHandler() {
		UpPressed = false;
	}

	private void LeftReleasedHandler() {
		LeftPressed = false;
	}

	private void RightReleasedHandler() {
		RightPressed = false;
	}

	private void UpPressedEventHandler() {
		UpPressed = true;
	}

	private void LeftPressedEventHandler() {
		LeftPressed = true;
	}

	private void RightPressedEventHandler() {
		RightPressed = true;
	}

	private float ClampAngle (float angle, float min, float max) {
		if (angle < -360F)
			angle += 360F;
		if (angle > 360F)
			angle -= 360F;

		return Mathf.Clamp (angle, min, max);
	}
}
