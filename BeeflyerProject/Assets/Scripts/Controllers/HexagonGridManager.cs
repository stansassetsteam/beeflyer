﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HexagonGridManager : MonoBehaviour {
	private int CurrentHexagon;

	public Transform CellsParent;
	public List<Transform> CellList = new List<Transform> ();
	public List<Transform> Hexagons = new List<Transform> ();

	private float[] Angles = {0f, 60f, 120f, 180f, 240f, 300f};

	void Awake() {
		HexagonSingleTrigger.HexagonTriggered += HandleHexagonTriggered;
	}

	void HandleHexagonTriggered (int index) {
		if (index == CurrentHexagon) {
			return;
		}

		CurrentHexagon = index;

		MoveGridTo (CurrentHexagon);
		SwapHexagons (CurrentHexagon);
	}

	private void MoveGridTo(int centrHexIndex) {
		CellsParent.transform.position = Hexagons [centrHexIndex].position;
	}

	private void SwapHexagons(int centrHexIndex) {
		List<int> HexForMoveArray = new List<int>();

		for (int i = 0; i < CellList.Count; i++) { //creating array with hex indexes for move
			if (i != centrHexIndex) {
				HexForMoveArray.Add(i);
			}
		}

		for (int i = HexForMoveArray.Count - 1; i > 0; i--) { //randomize hex for move array
			int r = Random.Range(0,i);
			int tmp = HexForMoveArray[i];
			HexForMoveArray[i] = HexForMoveArray[r];
			HexForMoveArray[r] = tmp;
		}

		int Index = 0;
		foreach(int cell in HexForMoveArray) { // swap positions
			Index += 1;

			Hexagons[cell].transform.position = CellList[Index].position;
			Hexagons[cell].transform.rotation = Quaternion.Euler(0f, Angles[Random.Range(0,5)], 0f);
		}
	}
}
	