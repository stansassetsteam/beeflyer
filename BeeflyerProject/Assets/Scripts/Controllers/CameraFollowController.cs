﻿using UnityEngine;
using System.Collections;

public class CameraFollowController : MonoBehaviour {

	private Transform CameraFollowObject;
	private Transform CameraLookAtObject;

	private bool IsPlayerExist = false;

	void Awake() {
		PlayerController.PlayerSpawned += PlayerSpawnedHandler;
		PlayerController.PlayerDestroyed += PlayerDestroyedHandler;
	}

	void PlayerDestroyedHandler() {
		IsPlayerExist = false;
	}

	void PlayerSpawnedHandler (Transform follow, Transform lookAt) {
		CameraFollowObject = follow;
		CameraLookAtObject = lookAt;

		IsPlayerExist = true;
		PlayerController.PlayerSpawned -= PlayerSpawnedHandler;
	}

	void Update() {
		if (IsPlayerExist) {

			transform.position = CameraFollowObject.position;
			transform.LookAt(CameraLookAtObject.position);
		}
	}
}
