﻿using UnityEngine;
using System.Collections;

public class HexagonSingle : MonoBehaviour {
	public int HexagonIndex;

	public GameObject AirHexagons;
	public GameObject GroundHexagons;

	void Awake () {
		RegisterAirColliders ();
	}

	private void RegisterAirColliders() {
		BoxCollider [] ColliderChildrens = AirHexagons.GetComponentsInChildren<BoxCollider> ();

		foreach (var child in ColliderChildrens) {
			child.gameObject.AddComponent<HexagonSingleTrigger>().SetParent(HexagonIndex);
		}
	}
}
