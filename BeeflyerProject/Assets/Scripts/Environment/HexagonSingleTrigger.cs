﻿using UnityEngine;
using System.Collections;
using System;

public class HexagonSingleTrigger : MonoBehaviour {

	private int PatrsParentIndex;
	public static event Action<int> HexagonTriggered;

	void OnTriggerEnter(){
		Debug.Log ("OnTriggerEnter");
		HexagonTriggered (PatrsParentIndex);
	}

	public void SetParent(int parIndex) {
		PatrsParentIndex = parIndex;
	}
}
