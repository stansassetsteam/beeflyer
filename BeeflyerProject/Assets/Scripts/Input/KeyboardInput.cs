﻿using UnityEngine;
using System.Collections;
using System;

public class KeyboardInputController : MonoBehaviour {
	public static event Action LeftPressedEvent = delegate {}; 
	public static event Action RightPressedEvent = delegate {};
	public static event Action UpPressedEvent = delegate {};

	public static event Action UpReleasedEvent = delegate {};
	public static event Action LeftReleasedEvent = delegate {};
	public static event Action RightReleasedEvent = delegate {};

	void Update() {
		if (Input.GetKey(KeyCode.A)) {
			LeftPressedEvent();
		}

		if (Input.GetKey(KeyCode.D)) {
			RightPressedEvent();
		}

		if (Input.GetKey(KeyCode.W)) {
			UpPressedEvent();
		}
		
		if (Input.GetKeyUp(KeyCode.W)) {
			UpReleasedEvent();
		}

		if (Input.GetKeyUp(KeyCode.A)) {
			LeftReleasedEvent();
		}

		if (Input.GetKeyUp(KeyCode.D)) {
			RightReleasedEvent();
		}
	}
}
